package com.example.root.practicasensores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
     Button btnSensores;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSensores = (Button)findViewById(R.id.btnSensores);

        btnSensores.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new  Intent (MainActivity.this,ActividadSensorAcelometro.class);
                startActivity(intent);
            }
        });
    }
}
